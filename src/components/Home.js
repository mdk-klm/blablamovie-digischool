import React from "react";
import popcorn from "../img/popcorn.png";
import { Layout, Typography } from "antd";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

export default function Home() {
  /*antd module package*/

  const { Header } = Layout;
  const TextTitle = Typography.Title;

  return (
    <div id="App">
      <div className="List">
        <Layout className="layout">
          <Header>
            <div style={{ textAlign: "center" }}>
              <TextTitle
                style={{ color: "#ffffff", marginTop: "14px" }}
                level={3}
              >
                BlablaMovie
              </TextTitle>
            </div>
          </Header>
        </Layout>
      </div>

      <div id="header">
        <div id="logo">
          <img alt="popcorn" src={popcorn}></img>
        </div>
        <div className="content">
          <div className="inner">
            <div>
              <ReactCSSTransitionGroup
                transitionName="anim"
                transitionAppear={true}
                transitionAppearTimeout={5000}
                transitionEnter={false}
                transitionLeave={false}
              >
                <p>Vote pour ton film préféré !</p>
              </ReactCSSTransitionGroup>
            </div>
          </div>
          <nav>
            <ul>
              <li>
                <a href="/concept">Concept</a>
              </li>
              <li>
                <a href="/listing">Listing</a>
              </li>
              <li>
                <a href="/easteregg">Surprise</a>
              </li>
            </ul>
          </nav>
        </div>
        <footer id="footer">
          <p class="copyright">&copy; Blablamovie pour Digischool</p>
        </footer>
      </div>
    </div>
  );
}
