import React from 'react';
import './App.css';
import Route from './components/Route.js';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


const App = () => {
  return (

   
    <div className="App">
      <header className="App-header">
      <Route/>
      </header>
    </div>
  );
  
}

export default App;
