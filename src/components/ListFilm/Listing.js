import React, { useEffect, useState } from "react";
import { Layout, Row, Alert, Modal, Typography } from "antd";
import "antd/dist/antd.css";
import { Button } from "antd";
import "../../App.css";
import ColFilm from "./ColFilm.js";
import FilmDetails from "./FilmDetails.js";
import SearchBar from "./SearchBar.js";
import Loader from "./Loader.js";

const Listing = () => {
  const apiKey = process.env.REACT_APP_BLABLAMOVIE_API_KEY;
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [q, setQuery] = useState("pirate");
  const [activateModal, setActivateModal] = useState(false);
  const [detail, setShowDetail] = useState(false);
  const [detailRequest, setDetailRequest] = useState(false);
  const { Header, Content, Footer } = Layout;
  const TextTitle = Typography.Title;

  useEffect(() => {
    fetch(`http://www.omdbapi.com/?s=${q}&apikey=${apiKey}`)
      .then((resp) => resp)
      .then((resp) => resp.json())
      .then((response) => {
        if (response.Response === "False") {
          setError(response.Error);
        } else {
          setData(response.Search);
        }
        setLoading(false);
      })
      .catch(({ message }) => {
        setError(message);
        setLoading(false);
      });
  }, [q]);

  return (
    /*Grid system by antd module package*/
    <div className="List">
      <Layout className="layout">
        <Header>
          <div style={{ textAlign: "center" }}>
            <TextTitle
              style={{ color: "#ffffff", marginTop: "14px" }}
              level={3}
            >
              BlablaMovie
            </TextTitle>
          </div>
        </Header>
        <Content style={{ padding: "0 50px" }}>
          <div
            style={{
              backgroundImage:
                "radial-gradient(rgba(0, 0, 0, 0.25) 25%, rgba(0, 0, 0, 0) 55%)",
              padding: 24,
              minHeight: 280,
            }}
          >
            <SearchBar searchHandler={setQuery} />
            <br />

            <Row gutter={16} type="flex" justify="center">
              {loading && <Loader />}

              {error !== null && (
                <div style={{ margin: "20px 0" }}>
                  <Alert message={error} type="error" />
                </div>
              )}

              {data !== null &&
                data.length > 0 &&
                data.map((result, index) => (
                  <ColFilm
                    ShowDetail={setShowDetail}
                    DetailRequest={setDetailRequest}
                    ActivateModal={setActivateModal}
                    key={index}
                    {...result}
                  />
                ))}
            </Row>
          </div>
          <Modal
            title="Detail"
            centered
            visible={activateModal}
            onCancel={() => setActivateModal(false)}
            footer={null}
            width={800}
          >
            {detailRequest === false ? <FilmDetails {...detail} /> : <Loader />}
          </Modal>
        </Content>
        <Footer style={{ textAlign: "center", background: "#282c34" }}>
          OMDB Movies ©2019
        </Footer>
      </Layout>
    </div>
  );
};

export default Listing;
