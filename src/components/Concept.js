import React from "react";
import "../App.css";
import { Layout, Typography, Button } from "antd";
import pirate from "../img/pirate.jpg";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

const Concept = () => {
  const { Header } = Layout;
  const TextTitle = Typography.Title;

  return (
    <div className="List">
      <Layout className="layout">
        <Header>
            <Button href="/home">Accueil</Button>
          <div style={{ textAlign: "center" }}>
            <TextTitle
              style={{ color: "#ffffff", marginTop: "14px" }}
              level={3}
            >
              BlablaMovie
            </TextTitle>
          </div>
        </Header>
      </Layout>
      <div>
        <ReactCSSTransitionGroup
          transitionName="anim"
          transitionAppear={true}
          transitionAppearTimeout={5000}
          transitionEnter={false}
          transitionLeave={false}
        >
          <h1 style={{ textAlign: "center", marginTop: "2rem" }}>
            BlablaMovie, qu'est-ce que c'est ?
          </h1>
        </ReactCSSTransitionGroup>
      </div>
      <div id="pirate">
        <img float="left" src={pirate}></img>
      </div>
      <div class="bubble bubble-bottom-left" contenteditable>
        Jeune mousaillon, ta mission sur BlablaMovie est de voter pour ton film
        de pirate préféré ! Il n'en restera qu'un a la fin!
      </div>

      <footer id="footer">
        <p class="copyright">&copy; Blablamovie pour Digischool</p>
      </footer>
    </div>
  );
};

export default Concept;
