import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Home from "../components/Home";
import Listing from "./ListFilm/Listing";
import Easteregg from "./ListFilm/Easteregg";
import Concept from "../components/Concept";

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/home" component={Home} />
        <Route path="/concept" component={Concept} />
        <Route path="/listing" component={Listing} />
        <Route path="/easteregg" component={Easteregg} />
        <Redirect from="/" to="/home" />
      </Switch>
    </BrowserRouter>
  );
}
